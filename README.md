![Build Status](https://gitlab.com/EAL-ITT/19A-ITT3-project/badges/master/pipeline.svg)


# 19A-ITT3-project

weekly plans, resources and other relevant stuff for the 3. semester projects work in IT technology 3. semester autumn of 2018 class.

public website for students:

*  [gitlab pages](https://eal-itt.gitlab.io/19A-ITT3-project/)