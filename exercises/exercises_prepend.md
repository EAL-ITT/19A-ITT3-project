---
title: '19A-ITT3-project'
subtitle: 'Exercises'
authors: ['Ruslan Trifonov \<rutr@ucl.dk\>','Ilas Esmati \<iles@ucl.dk\>']
main_author: 'Ilas Esmati '
date: \today
email: 'iles@ucl.dk'
left-header: \today
right-header: '19A-ITT3-project, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans. 

References

* [Weekly plans](https://eal-itt.gitlab.io/19A-ITT3-project/19A-ITT3-project_weekly_plans.pdf)



