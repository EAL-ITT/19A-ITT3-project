---
Week: 40
Content:  Project startup
Material: See other gitlab project
Initials: RUTR
---

# Week 40 ITT3 Project - Drone sensory project

This document is only for the students that are working on, the syd dynamics project.

## Goals of the week(s)
Pratical and learning goals for the period is as follows:
1. Students will create a plan together with Ruslan.
2. The team will assigned goals and start work on the issues.
3. the TPM sensor will be picked up.
4. The submarine project, white paper will be complited.
5. Understand noise in gyroscopes.

### Practical goals

* Taks assigment for teams for experts.
* pick up sensors.
* start interfacing of TPM sensors.
* SMART task profile.


### Learning goals

- Allan variance, using mathlab and python.

## Deliverables

* Project plan.

## Schedule

		Meeting in Project Room - 08:30
		Ilas and Ruslan will figure out work for first semester 09:00    

   
## Hands-on time

- Handin project outline and plan - full version, tasks, and deliverables included.

## Comments
* Read about SMART tasks [here](https://gigaom.com/2008/05/05/how-to-create-smart-tasks/)
