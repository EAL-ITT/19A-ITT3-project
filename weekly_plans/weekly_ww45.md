---
Week: 45
Content:  Weekly Plan ww45
Material: See other gitlab project
Initials: RUTR/ILES
---

# Week 45 ITT3 Project


## Goals of the week(s)

Piratical and learning goals for the period is as follows:

1. Teacher meetings, we meet at B2.49/B2.05.
2. Documentation for Phase Two, outlines submission. First skeletal draft.
3. Main systems, connected.
4. Test of TCP/IP and other external communications. It is expected that all internal communication is working.

### Practical goals

* Teach meetings.
* Documentation: start of phase two report
* Communication test.
* Test specification, test report, and success criteria.

### Learning goals

- Report
- Testing

## Deliverable

* Gitlab, updated with report, and test protocols.

## Schedule

		* Meetings start at 09:00 in B2.49. Ilas may change this. Pay attention to Its Learning.
		* Extra meetings for issues are held at 15:00 in e-lab, Ilas and Ruslan are pressented.
   
## Hands-on time

- Gitlab updated.

## Comments
* Read about SMART tasks [here](https://gigaom.com/2008/05/05/how-to-create-smart-tasks/)


