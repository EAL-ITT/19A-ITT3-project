---
Week: 47
Content:  Weekly Plan ww47
Material: See other gitlab project
Initials: RUTR/ILES
---

# Week 47 ITT3 Project


## Goals of the week(s)

Piratical and learning goals for the period is as follows:

1. Teacher meetings, we meet at B2.49/B2.05
2. Report upgrade
3. PCB design
 
### Practical goals

* First Iteration of PCB design. It is advisable that students have something by the end of the week.
* Report upgrade. Verification with Ilas.
* Sanity check on PCB design with Ilas



### Learning goals

- Verification of PCB design.
- Report second iteration.

## Deliverable

* Iteration three of documentation and second report.
* PCB diagram.

## Schedule

		* Meetings start at 09:00 in B2.49. Ilas may change this. Pay attention to Its Learning.
		* Extra meetings for issues are held at 15:00 in e-lab, Ilas and Ruslan are pressented.  

   
## Hands-on time

- Mark Three of documentation.
- Mark One of PCB diagram

## Comments
* Read about SMART tasks [here](https://gigaom.com/2008/05/05/how-to-create-smart-tasks/)

