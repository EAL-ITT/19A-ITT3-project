---
Week: 43
Content:  Weekly Plan ww43
Material: See other gitlab project
Initials: RUTR/ILES
---

# Week 43 ITT3 Project


## Goals of the week(s)

Piratical and learning goals for the period is as follows:

1. All work from earlier weeks is concluded.
2. Groups are ready for phase two.
3. Demonstrate with block diagram the complete functionality and timing of the system’s software as a whole.
4. Demonstrate first iteration of project.



### Practical goals

* Teach meetings.
* Exam.
* Start Phase two. Outline goals and deliverable with Ilas.


### Learning goals

- Getting a good grade at the exam
- Phase shift in project. Conclude deliverable and proceed.

## Deliverable

* Exam.


## Schedule

		* Meetings start at 09:00 in B2.49. Ilas may change this. Pay attention to Its Learning.
		* Extra meetings for issues are held at 15:00 in e-lab, Ilas and Ruslan are pressented.  

   
## Hands-on time

N/A

## Comments
* Read about SMART tasks [here](https://gigaom.com/2008/05/05/how-to-create-smart-tasks/)

