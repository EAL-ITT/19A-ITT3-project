---
Week: 44
Content:  Weekly Plan ww44
Material: See other gitlab project
Initials: RUTR/ILES
---

# Week 44 ITT3 Project


## Goals of the week(s)

Pratical and learning goals for the period is as follows:

1. Teacher meetings, we meet at B2.49/B2.05.
2. setting setup and implement first iteration of hardware and software.
3. Testing line or other navigation methods.
4. motor control is up and running, PID is set up and is been tuned.
 
### Practical goals

* Teach meetings.
* Set up test.
* Control mode ready and tested.


### Learning goals

- PID.
- Control Theory, if needed explaind by Ilas.

## Deliverables

* Ilas may ask for something special.

## Schedule

		* Meetings start at 09:00 in B2.49. Ilas may change this. Pay attention to Its Learning.
		* Extra meetings for issues are held at 15:00 in e-lab, Ilas and Ruslan are pressented.

   
## Hands-on time

- Gitlab needs to be updated.

## Comments
* Read about SMART tasks [here](https://gigaom.com/2008/05/05/how-to-create-smart-tasks/)

