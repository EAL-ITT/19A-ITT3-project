---
Week: 4
Content:  Weekly Plan ww42
Material: See other gitlab project
Initials: RUTR/ILES
---

# Week 42 ITT3 Project


## Goals of the week(s)

Piratical and learning goals for the period is as follows:

1. Catch up
 
### Practical goals

* Relax
* Work extra
* Catch up on left over work

### Learning goals

- extra work, that is not credited.

## Deliverable

* Self passing

## Schedule

		Meeting free week

   
## Hands-on time

- Handin project report - full version of part 1, tasks, and deliverable included. As well as all the block diagrams.

## Comments
* Read about SMART tasks [here](https://gigaom.com/2008/05/05/how-to-create-smart-tasks/)

