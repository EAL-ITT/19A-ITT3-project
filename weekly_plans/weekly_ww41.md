---
Week: 41
Content:  Weekly Plan ww41
Material: See other gitlab project
Initials: RUTR/ILES
---

# Week 41 ITT3 Project


## Goals of the week(s)

Piratical and learning goals for the period is as follows:

1. Teacher meetings, we meet at B2.49/B2.05.
2. Wireless Charger results. What is best in market. Or Best Components.
What have you chosen to work with.
3. Code Block Diagram, Presentation and Implementation. See `Hints' for more.
4. If Code Block Diagram for the process and hardware are done. Do the rest  with respect to software, and communication.
 
### Practical goals

* Teach meetings.
* Status of team from first semester. Make sure that first semester are on track
During the Presentation ask Ilas for first semester status. So that you will not
wast time.
* Work on report, and submission. As Ilas for guidance.
* Extra meetings for issues are held at 15:00 in e-lab.


### Learning goals

- Block diagrams.
- Report.

## Deliverable

* Report, individual on wiseflow. We expect that you will have a lot of plagiarisms. Since it is group work.

## Schedule

		* Meetings start at 09:00 in B2.49. Ilas may change this. Pay attention to Its Learning.
		* Extra meetings for issues are held at 15:00 in e-lab, Ilas and Ruslan are pressented.
  

   
## Hands-on time

- Handin project report - full version of part 1, tasks, and deliverable included. As well as all the block diagrams.

## Comments
* Read about SMART tasks [here](https://gigaom.com/2008/05/05/how-to-create-smart-tasks/)


## Hints

The robot vehicle  retrieve information about all the charging stations’ status, and navigate to a free charging station autonomously for wireless power transfer.

### Hint 1:

The script accepts an arbitrary amount of IP addresses and its own TCP port as additional arguments. When the script is run, the script sets up the vehicle as a TCP client and attempts to connect to each specified IP address representing the charging stations in the network. If a connection to the first specified IP is unsuccessful, the vehicle attempts to connected to the second IP address, and so on.

### Hint 2:

If the attempted connection is successful, the script then sends a “Charge Request” message to the station server. After successful transmission of the request, the vehicle client should receive the free charging station’s ID number as a response. 
Depending on the ID number received, the robot vehicle will adjust its direction of travel to either the left or right side of the black line, if it is placed perpendicular to the line in such a way that all sensors are pointing at a black colored surface. After the vehicle has adjusted its direction, it initializes and runs a PID controller algorithm to follow the line.
