---
Week: 48
Content:  Weekly Plan ww48
Material: See other gitlab project
Initials: RUTR/ILES
---

# Week 48 ITT3 Project


## Goals of the week(s)

Piratical and learning goals for the period is as follows:

1. Teacher meetings, we meet at B2.49/B2.05.
2. Final Test.
3. Final test documentation.
4. Final Test Results.
5. PCB design done.
 
### Practical goals

* Final version of all documentation.
* PCB to be send for print. It can take two weeks to print and ship. Be careful.



### Learning goals

- PCB Manufacturing process.

## Deliverable

* Final version of all documentation.
* PCB.
* Work on final report.

## Schedule

		* Meetings start at 09:00 in B2.49. Ilas may change this. Pay attention to Its Learning.
		* Extra meetings for issues are held at 15:00 in e-lab, Ilas and Ruslan are pressented.

   
## Hands-on time

- Final version on everything should be on gitlab.

## Comments
* Read about SMART tasks [here](https://gigaom.com/2008/05/05/how-to-create-smart-tasks/)

