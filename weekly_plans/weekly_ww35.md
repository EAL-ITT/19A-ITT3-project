---
Week: 35
Content:  Project startup
Material: See links in weekly plan
Initials: ILES/RUTR
---

# Week 35 ITT3 Project

## Goals of the week(s)
Pratical and learning goals for the period is as follows:
-	Groups make Block diagram of the overall system
-	communication between vehicle and stations. 
-	Task division within the groups
-   Each group makes a git-lab project page and include teachers

[gitlab pages](https://eal-itt.gitlab.io/19A-ITT3-project/)

- Each group presents thier project through story telling


### Practical goals

* Groups have been formed
### Practical goals Thursday and Friday


Thursday:
0815-1115
-	Students design an experiment to demonstrate the concept of wireless charging using induction.
Hint ( design a transmitter circuit and receiver circuit using the components from E-lab. You can make your own coil using enameled copper wire. Remember to scrap off the insulation on the terminals. You can use an LED or a motor to show wireless electricity transmission)

1215
-	Students show their ideas/sketches and wireless charging circuit to Ilias Esmati 
-	Students refine and re-design their circuit if necessary 
-	Students send their designed receiver/transmitter circuit to iles@ucl.dk for approval latest 1530


Friday:
0815-1115
-	Students build and test their approved receiver/transmitter circuit on bread board

1530
-	Students send a full documentation of their lab work and experiment with explanations and pictorial demonstrations of their setup and circuit.




### Learning goals
- students will learn about induction 

## Deliverables

* gitlab user account created
- sketches are uploaded to git lab
## Schedule



        Curriculum
        Expectations (OLA, reports, presentations)
        Buddies and groups
        Project phases
        Project plan
        Project management (Gitlab)
        Schedule structure

    

   
## Hands-on time

* Exercise 0: gitlab workshop

    We will explain what our project management platform, gitlab, is and how you will use it for the project.

    1. Gitlab overview and features
    2. Version control
    3. SSH keys setup
    4. Gitlab codelabs
    


## Comments
* Read about SMART tasks [here](https://gigaom.com/2008/05/05/how-to-create-smart-tasks/)
