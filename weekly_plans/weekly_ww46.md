---
Week: 46
Content:  Weekly Plan ww46
Material: See other gitlab project
Initials: RUTR/ILES
---

# Week 46 ITT3 Project


## Goals of the week(s)

Piratical and learning goals for the period is as follows:

1. Teacher meetings, we meet at B2.49/B2.05.
2. Documentation control.
3. Optimization of design.
4. All goals so far will be concluded and there is an overview.
 
### Practical goals

* Teach meetings.
* Documentation. Sanity check of documentation with Ilas.
* Test results.
* Test protocols on gitlab.


### Learning goals

- Verification of own work.
- Cross check of work.

## Deliverable

* Iteration two of documentation and report.

## Schedule

		* Meetings start at 09:00 in B2.49. Ilas may change this. Pay attention to Its Learning.
		* Extra meetings for issues are held at 15:00 in e-lab, Ilas and Ruslan are pressented.

   
## Hands-on time

- Mark Two of documentation.

## Comments
* Read about SMART tasks [here](https://gigaom.com/2008/05/05/how-to-create-smart-tasks/)

