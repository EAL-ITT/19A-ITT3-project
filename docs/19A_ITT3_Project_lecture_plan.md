---
title: '19A ITT3 Project'
subtitle: 'Lecture plan'
authors: ['Ruslan Trifonov \<rutr@ucl.dk\>', 'Ilas Esmati \<iles@ucl.dk\>']
main_author: 'Ilas Esmati'
date: \today
email: 'iles@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, oeait18, 19A
* Name of lecturer and date of filling in form: ILES, 2019-08-01
* Title of the course, module or project and ECTS: Embedded Systems, ? ECTS
* Required readings, literature or technical instructions and other background material: None

See weekly plan for details like detailed daily plan, links, references, exercises and so on.


-------- ------ ---------------------------------------------
 INIT     Week  Content
-------- ------ ---------------------------------------------
ILES
---------------------------------------------------------


# General info about the course, module or project
The Course will be focusing on practical skill and supplementary theoretical knowledge.
Hands on first with basic theory, followed but in-depth theory via focused needs. The students will have all the available information about the course overview from study start. Material will be shared in advance and the students are asked to investigate the topics and prepare from home.
In the classroom a short presentation will be given followed by practical work in small groups.

## The student’s learning outcome

### Knowledge

The students will acquire into communication and interface equipment. With applied solutions. How electronic modules are contracted and used. Communication protocol (SPI). IoT technologies applied on Linux operating system. Signal interfacing and usage.

### Skills

The students will focus on learning practical skills with focus on applied embedded systems, for example analog signal handling and DC motor interfacing. Testing their solution and documenting it via gitlab.

### Competencies

Students will be build their competencies on testing and analysis of embedded solutions, taking into account their environmental foot print,

## Content

The detailed content is given into the weekly plans document. Three major topics are the focus. Building simple embedded reactive circuit. Interfacing analog input via an external converter and into a raspberry pi. Lastly interfacing a DC motor into raspberry pi and controlling it with external inputs.

## Method

The method of teaching will be a flipped classroom. Students are expected to search and learn on their own in advance following documentation provided by the lecture and their own sources. In the class short power point presentations will be given with focus knowledge and targeting the needs of the classroom. After that the students will be spend their time practicing their skills with the guidance of a lecturer.

## Equipment

The equipment needed for this course is a personal computer, a raspberry pi 3 B+ or higher. The course will be moving to raspberry pi 4 in November. Some basic components provided from by UCL as well as some measurement equipment.

## Projects with external collaborators  (proportion of students who participated)

The mini project are all internal. For the first semester in embedded systems. If students have a company that would like to cooperate with UCL and the task fits the curriculum of 2018 an evaluation can be done.

## Test form/assessment
The course includes 1 compulsory elements. In week 39 and an alternative on in week 43. Please note that students have more time to deliver. Their usually have 2 weeks but the delivery dates are in week 39 and week 43 via wiseflow.

See exam cataloger for details on compulsory elements, and weekly plans document for tasks planned.

## Other general information
Students are expected to work and ask questions. The course structure may fill that the difficulty oscillates. This is due to the course been build around modules and units of topics.
