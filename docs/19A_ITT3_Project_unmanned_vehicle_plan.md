---
title: '19A ITT3 Project Plan'
subtitle: 'Unmanned vehicle project plan'
authors: ['Ruslan Trifonov \<rutr@ucl.dk\>', 'Ilas Esmati \<iles@ucl.dk\>']
main_author: 'Ruslan Trifonov'
date: \today
email: 'rutr@ucl.dk'
left-header: \today
right-header: Unmanned vehicle project plan
skip-toc: false
---


# Project plan

The project plan consisted off weekly planned milestones and tasks as well as deliverables.

* Study program, class and semester: IT technology, oeait18, 19A
* Name of lecturer and date of filling in form: RUTR, 2019-09-09
* Title of the course, module or project and ECTS: Project, 10 ECTS
* Required readings, literature or technical instructions and other background material: None

See weekly plan for details like detailed daily plan, links, references, exercises and so on.


-------- ------ ---------------------------------------------
INIT     Week  Content
-------- ------ ---------------------------------------------
RUTR
---------------------------------------------------------


# General info about the course, module or project
This project will focus on build a solution for Copenhagen Technologies with respect to theirs unmanned underwater vehicle idea.
The task is too develop a remote control unmanned underwater vehicle with an array of sensors and a high quality upload link and video streaming capabilities. The project will be considered a success when the following criteria are meet:

* Remote controlled vehicle that operates underwater. (priority 1)
* Video link - live video streaming. (priority 1)
* Sensory array, data transmission in real time of various onboard sensor to be defined at a later stage. (priority 2)
* Powered via batter. (priority 1)
* Wireless recharging. (priority 3)

Task should be work according to priorities, with 1 been the highest and 3 the lowest.

## Time Frame of Project

### Phase one

Third semester students will start developing up-link and data transfer. The requirements are:

1. Video need to work under water.
2. Data transfer has to be batter power friendly.
3. Video transfer has to be stable.


### Phase two

First semester students will be more active in this phase, they will need to research or/and build a submarine. The following specification need to be in place:

1. Space for the electronics and sensors, plus batter charging and extra battery.
2. Additional antenna, that is water proof.


# Weekly plan overview

## Week 37
Research of video transmission solutions:

1. Depth penetration.
2. Frequency.
3. Quality of video.

Research of underwater equipment:

1. Underwater kit/vehicle.

## Week 38

Research of video transmission solutions:

1. Depth penetration.
2. Frequency.
3. Quality of video.

Research of underwater equipment:

1. Underwater kit/vehicle.


## Week 39

## Week 40


## Week 41

## Week 42

## Week 43

## Week 44


## Week 45

## Week 46

## Week 47

## Week 48

