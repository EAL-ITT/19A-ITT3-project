---
title: '19A ITT1 Project'
subtitle: 'Project plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Ilias Ismati \<iles@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: Project plan
skip-toc: false
---



# Background

This is the semester project for ITT1 where you will work with different projects, some of them initated by companies. This projectplan will cover the project, and will match topics that are taught in parallel classes.


# Purpose

The main goal is to ....
This is a learning project and the primary objective is for the students to get a good understanding of the challenges involved in making .... and to give them hands-on experience with the implementation.

For simplicity, the goals stated wil evolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.


# Goals

The overall system that is going to be build looks as follows

<insert block diagram>

<explanation of system diagram>

Project deliveries are:
* A
* B
* C
* etc.

The project itself will be divided into 3 phases and gitlab will be used as the project management platform.


# Schedule

The project is divided into three phases from now to christmas holidays.

See the lecture plan for details.


# Organization

[Considerations about the organization of the project may includes
    • Identification of the steering committee
    • Identification of project manager(s)
    • Composition of the project group(s)
    • Identification of external resource persons or groups

For each of the identified groups and people, their tasks must be specified and their role in the project must be clear. This could be done in relation to the goals of the project.]

TBD: This section must be completed by the students.


# Budget and resources

No monetary resources are expected. In terms of manpower, only the people in the project group are expected to contrbute,


# Risk assessment

[The risks involved in the project is evaluated, and the major issues will be listed. The plan will include the actions taken as part of the project design to handle the risk or the actions planned should a given risk materialize.]

TBD: This section must be completed by the students based on the pre-mortem meeting


# Stakeholders

[An analysis of the stakeholder is conducted to establish who has an interest in the project. There will be multiple stakeholders with diverse interests in the project.

Possible stakeholders
    • Internal vs. external
    • Positive vs. negative
    • Active vs. passive
A strategy could be planned on how to handle each stakeholder and how to handle stakeholders different (and/or conflicting) interests and priorities.
This could also include actions designed to transform a person or a group into a (positive) stakeholder, or increase the value of the project for a given stakeholder.]

TBD: This section is to be completed by the students.
TBD: Please consider finding a relevant external stakeholder.

# Communication

[The stakeholders may have some requirements or requests as to what reports or other output is published or used. Some part of the project may be confidential.
Whenever there is a stakeholder, there is a need for communication. It will depend on the stakeholder and their role in the project, how and how often communication is needed.
In this section, reports, periodic emails, meetings, facebook groups and any other stakeholder communication will be described]

TBD: Students fill out this part based on risk assessment and stakeholders.

# Perspectives

This project will serve as a template for other similar projects in future semesters.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

[Evaluation is about how to gauge is the project was successful. This includes both the process and the end result. Both of which may have consequences on future projects.
The will be some documentation needed at the end (or during) the project, e.g. external funding will require some sort of reporting after the project has finished. This must be described.]

TBD: Students fill out this one also

# References

[Include any relevant references to legal documents, literature, books, home pages or other material that is relevant for the reader.]

TBD: Students may add stuff here at their discretion
TBD: or just write "None at this time"
